package com.wg.test.model.pojo;

import com.wg.test.model.LatencySign;
import com.wg.test.model.SkillSign;
import com.wg.test.model.dto.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Класс-POJO, содержащий информацию об игроке, находящемся в группе
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class MatchUser extends User {
	/**
	 * Имя игрока
	 */
	private String name;

	/**
	 * Уровень "скилла" игрока
	 */
	private double skill;

	/**
	 * Уровень "задержки" игрока
	 */
	private double latency;

	/**
	 * Время помещения игрока в группу
	 */
	private LocalDateTime createdAt;

	/**
	 * Обозначение "скилла" игрока {@link SkillSign} для подбора оптимальной группы
	 */
	private SkillSign skillSign;

	/**
	 * Обозначение задержки игрока {@link LatencySign} для подбора оптимальной группы
	 */
	private LatencySign latencySign;

	public MatchUser(User user) {
		this.name = user.getName();
		this.skill = user.getSkill();
		this.latency = user.getLatency();
		this.createdAt = LocalDateTime.now();
		this.skillSign = defineSkillSign();
		this.latencySign = defineLatencySign();
	}

	/**
	 * Определение обозначения "скилла" игрока в зависимости от заданного параметра
	 */
	private SkillSign defineSkillSign() {
		double skill = getSkill();

		if (skill >= SkillSign.HIGH.getValue()) {
			return SkillSign.HIGH;
		} else if (skill >= SkillSign.GOOD.getValue()) {
			return SkillSign.GOOD;
		} else if (skill >= SkillSign.MEDIUM.getValue()) {
			return SkillSign.MEDIUM;
		} else if (skill >= SkillSign.BAD.getValue()) {
			return SkillSign.BAD;
		} else {
			return SkillSign.LOW;
		}
	}

	/**
	 * Определение обозначения "задержки" игрока в зависимости от заданного параметра
	 */
	private LatencySign defineLatencySign() {
		double latency = getLatency();

		if (latency < LatencySign.LOW.getValue()) {
			return LatencySign.LOW;
		} else if (latency < LatencySign.GOOD.getValue()) {
			return LatencySign.GOOD;
		} else if (latency < LatencySign.MEDIUM.getValue()) {
			return LatencySign.MEDIUM;
		} else if (latency < LatencySign.BAD.getValue()) {
			return LatencySign.BAD;
		} else {
			return LatencySign.HIGH;
		}
	}
}
