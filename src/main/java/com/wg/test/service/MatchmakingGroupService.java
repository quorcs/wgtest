package com.wg.test.service;

import com.wg.test.model.pojo.MatchmakingGroup;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Класс, содержащий информацию о группах игроков
 */
public class MatchmakingGroupService {

	/**
	 * Значение, показывающее текущий идентификатор для группы (т.к. они все должны быть последовательными)
	 */
	private static final AtomicLong currentGroupId = new AtomicLong(0);

	/**
	 * Общий пулл групп, которые еще не собраны для начала матча
	 */
	private static final Map<String, MatchmakingGroup> groupsPool = new ConcurrentHashMap<>();

	/**
	 * Добавление группы в общий пулл
	 */
	public static void addGroupInPool(MatchmakingGroup group) {
		groupsPool.put(group.getGroupSign(), group);
	}

	/**
	 * Получение группы из общего пулла посредством groupSign
	 */
	public static MatchmakingGroup getGroup(String groupSing) {
		return groupsPool.get(groupSing);
	}

	/**
	 * Удаление группы из общего пулла (происходит, когда группа укомплектована)
	 */
	public static void removeFromPool(String groupSign) {
		groupsPool.remove(groupSign);
	}

	/**
	 * Получение идентификатора для новой группы
	 */
	public static long getNextGroupId() {
		return currentGroupId.incrementAndGet();
	}
}
