package com.wg.test.model.pojo;

import com.wg.test.model.dto.User;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Класс-POJO, содержащий информацию о конкретной группе (матче)
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MatchmakingGroup {
	/**
	 * Строковый идентификатор группы, по которому подбираются игроки
	 */
	private String groupSign;

	/**
	 * Уникальный числовой идентификатор группы
	 */
	private long groupId;

	/**
	 * Количество игроков в группе в текущий момент
	 */
	private int usersCount;

	/**
	 * Список всех игроков в группе в текущий момент
	 */
	private final List<MatchUser> users = new CopyOnWriteArrayList<>();

	/**
	 * Время создания группы (матча)
	 */
	private LocalDateTime createdAt;

	/**
	 * Время начала игры для группы (время старта матча)
	 */
	private LocalDateTime startedAt;

	/**
	 * Добавление игрока в список игроков группы
	 * Заодно увеличиваем значение userCount
	 */
	public void addUser(MatchUser matchUser) {
		this.users.add(matchUser);
		this.usersCount++;
	}

	/**
	 * Получение информации о "скилле" игроков группы в строковом виде для вывода в консоль
	 */
	public String getSkillInfo() {
		return compareSkill(false) +
				"/" +
				compareSkill(true) +
				"/" +
				getAvgGroupSkill() +
				" skill in group";
	}

	/**
	 * Вычисление максимального/минимального значения "скилла" среди всех игроков группы
	 */
	private Double compareSkill(boolean max) {
		Comparator<MatchUser> comparator = Comparator.comparing(MatchUser::getSkill);
		Optional<MatchUser> optionalMatchUser = max ? users.stream().max(comparator) : users.stream().min(comparator);

		return optionalMatchUser.map(User::getSkill).orElse(0.);
	}

	/**
	 * Вычисление среднего значения "скилла" игроков группы
	 */
	private Double getAvgGroupSkill() {
		double sumGroupSkill = users.stream().mapToDouble(MatchUser::getSkill).sum();
		return sumGroupSkill / usersCount;
	}

	/**
	 * Получение информации о "задержке" игроков группы в строковом виде для вывода в консоль
	 */
	public String getLatencyInfo() {
		return compareLatency(false) +
				"/" +
				compareLatency(true) +
				"/" +
				getAvgGroupLatency() +
				" latency in group";
	}

	/**
	 * Вычисление максимального/минимального значения "задержки" среди всех игроков группы
	 */
	private Double compareLatency(boolean max) {
		Comparator<MatchUser> comparator = Comparator.comparing(MatchUser::getLatency);
		Optional<MatchUser> optionalMatchUser = max ? users.stream().max(comparator) : users.stream().min(comparator);

		return optionalMatchUser.map(User::getLatency).orElse(0.);
	}

	/**
	 * Вычисление среднего значения "задержки" игроков группы
	 */
	private Double getAvgGroupLatency() {
		double sumGroupLatency = users.stream().mapToDouble(MatchUser::getLatency).sum();
		return sumGroupLatency / usersCount;
	}

	/**
	 * Получение информации о времени ожидания в очереди игроков группы в строковом виде для вывода в консоль
	 */
	public String getTimeInQueueInfo() {
		return compareTimeInQueue(true) +
				"/" +
				compareTimeInQueue(false) +
				"/" +
				getAvgGroupTimeInQueue() +
				" time spent in queue";
	}

	/**
	 * Вычисление максимального/минимального значения времени ожидания в очереди среди всех игроков группы
	 */
	private Double compareTimeInQueue(boolean min) {
		Comparator<MatchUser> comparator = Comparator.comparing(MatchUser::getCreatedAt);
		Optional<MatchUser> optionalMatchUser = min ? users.stream().max(comparator) : users.stream().min(comparator);

		return optionalMatchUser.isPresent() ?
				ChronoUnit.SECONDS.between(getStartedAt(), optionalMatchUser.get().getCreatedAt()) :
				0.;
	}

	/**
	 * Вычисление среднего времени ожидания в очереди игроков группы
	 */
	private Double getAvgGroupTimeInQueue() {
		double sumGroupTimeInQueue = users
				.stream()
				.mapToDouble(u -> ChronoUnit.SECONDS.between(getStartedAt(), u.getCreatedAt()))
				.sum();
		return sumGroupTimeInQueue / usersCount;
	}

	/**
	 * Получение списка имен всех игроков группы для вывода в консоль
	 */
	public String getUsersNames() {
		StringBuilder stringBuilder = new StringBuilder();
		users.forEach(u -> stringBuilder.append(u.getName()).append(", "));
		return stringBuilder.toString();
	}
}
