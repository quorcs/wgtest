package com.wg.test.model.dto;

import lombok.*;

/**
 * Класс-DTO, содержащий информацию об игроке,
 * которая необходима для определения игрока в группу
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class User {
	/**
	 * Имя игрока
	 */
	private String name;

	/**
	 * Уровень "скилла" игрока
	 */
	private double skill;

	/**
	 * Уровень "задержки" игрока
	 */
	private double latency;
}
