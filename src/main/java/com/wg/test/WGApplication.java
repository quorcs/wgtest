package com.wg.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WGApplication {

	public static void main(String[] args) {
		SpringApplication.run(WGApplication.class, args);
	}

}
