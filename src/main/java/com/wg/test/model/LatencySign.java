package com.wg.test.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Обозначения "задержки" игрока
 * Предназначается для разделения игроков на группы
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum LatencySign {
	/**
	 * Высокий уровень "задержки"
	 */
	HIGH,

	/**
	 * Плохой уровень "задержки"
	 */
	BAD(120),

	/**
	 * Средний уровень "задержки"
	 */
	MEDIUM(80),

	/**
	 * Хороший уровень "задержки"
	 */
	GOOD(40),

	/**
	 * Низкий уровень "задержки"
	 */
	LOW(20);

	private double value;
}
