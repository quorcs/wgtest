package com.wg.test.controller;

import com.wg.test.model.dto.User;
import com.wg.test.service.MatchmakingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.ResponseEntity.ok;

/**
 * Контроллер, содержащий запросы для матчмейкинга
 */
@RestController
@RequiredArgsConstructor
public class MatchmakingController {

	private final MatchmakingService matchmakingService;

	/**
	 * Запрос на попадание в пул матчмейкинга
	 */
	@PostMapping("/users")
	public ResponseEntity<Void> addToMatch(@RequestBody User userRequest) {
		matchmakingService.addToGroup(userRequest);
		return ok().build();
	}
}
