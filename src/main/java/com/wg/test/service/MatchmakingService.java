package com.wg.test.service;

import com.wg.test.model.dto.User;

/**
 * Интерфейс, содержащий методы для работы с матчмейкингом
 */
public interface MatchmakingService {

	/**
	 * Добавление игрока в группу
	 */
	void addToGroup(User user);
}
