package com.wg.test.service;

import com.wg.test.model.pojo.MatchUser;
import com.wg.test.model.pojo.MatchmakingGroup;
import com.wg.test.model.dto.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Класс, реализующий интерфейс {@link MatchmakingService}
 */
@Slf4j
@Service
public class MatchmakingServiceImpl implements MatchmakingService {

	/**
	 * Максимальное количество игроков в группе
	 */
	@Value("${matchmaking.group.max.size}")
	private int groupMaxSize;

	/**
	 * Добавление игрока в группу
	 */
	@Override
	public void addToGroup(User user) {
		MatchmakingGroup group = addUserToGroupAndGet(new MatchUser(user));
		checkGroupForStarting(group);
	}

	/**
	 * Добавление игрока в конкретную группу и получение данной группы
	 */
	private MatchmakingGroup addUserToGroupAndGet(MatchUser matchUser) {
		MatchmakingGroup group = MatchmakingGroupService.getGroup(createGroupSign(matchUser));

		// если подходящей группы не оказалось в общем пулле групп,
		// то создаем новую группу и добавляем ее в общий пулл
		if (group == null) {
			group = createGroup(matchUser);
			MatchmakingGroupService.addGroupInPool(group);

			log.info("Group with ID {} was created", group.getGroupId());
		}

		// добавляем игрока в группу
		group.addUser(matchUser);

		log.info("User {} was added to group with ID {}", matchUser.getName(), group.getGroupId());

		return group;
	}

	/**
	 * Создание новой группы с текущим игроком
	 */
	private MatchmakingGroup createGroup(MatchUser matchUser) {
		MatchmakingGroup matchmakingGroup = new MatchmakingGroup();

		matchmakingGroup.setGroupId(MatchmakingGroupService.getNextGroupId());
		matchmakingGroup.setCreatedAt(LocalDateTime.now());
		matchmakingGroup.setGroupSign(createGroupSign(matchUser));

		return matchmakingGroup;
	}

	/**
	 * Создание обозначения группы, по которому можно легко находить подходящих игроков
	 * Пример обозначения: skill_medium_latency_low
	 * Следовательно, игроки автоматически балансятся по "скиллу" и "задержке"
	 */
	public String createGroupSign(MatchUser user) {
		return "skill_" + user.getSkillSign() + "_latency_" + user.getLatencySign();
	}

	/**
	 * Как только группа становится укомплектованной,
	 * то удаляем ее из общего пулла групп (якобы матч стартовал)
	 * и выводим в консоль информацию о группе
	 */
	private void checkGroupForStarting(MatchmakingGroup group) {
		if (group.getUsersCount() == groupMaxSize) {
			group.setStartedAt(LocalDateTime.now());

			MatchmakingGroupService.removeFromPool(group.getGroupSign());

			printGroupInfo(group);
		}
	}

	/**
	 * Вывод в консоль информации о группе
	 */
	private void printGroupInfo(MatchmakingGroup group) {
		log.info("Group ID: " + group.getGroupId());
		log.info("Min / Max / Avg");
		log.info(group.getSkillInfo());
		log.info(group.getLatencyInfo());
		log.info(group.getTimeInQueueInfo());
		log.info("Users names: " + group.getUsersNames());
	}
}
