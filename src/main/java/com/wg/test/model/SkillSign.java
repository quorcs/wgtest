package com.wg.test.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Обозначение "скилла" игрока
 * Предназначается для разделения игроков на группы
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum SkillSign {
	/**
	 * Высокий уровень "скилла"
	 */
	HIGH(850),

	/**
	 * Хороший уровень "скилла"
	 */
	GOOD(600),

	/**
	 * Средний уровень "скилла"
	 */
	MEDIUM(400),

	/**
	 * Плохой уровень "скилла"
	 */
	BAD(200),

	/**
	 * Низкий уровень "скилла"
	 */
	LOW;

	private double value;
}
